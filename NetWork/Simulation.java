package NetWork;

import java.io.File;
import java.text.NumberFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import odeToJava.Erk;
import odeToJava.modules.Btableau;
import odeToJava.modules.Span;
import IoTools.Analyser;
import IoTools.XlsExporter;

/**
 *
 * @author Clallier & AFournel
 */
public class Simulation implements Runnable {

    /**
     * @param args the command line arguments
     */
    private static double step = 0.05D;
    private static MainWindow win;
    private static boolean patient;
    private static boolean done;
    private static int number;

    /**
     * 
     */
    public void run() {
        win = new MainWindow();
        win.setVisible(true);

        patient = true;
        done = false;

        int compteur = 0;

        while (!done) {
            win.setInfoLabel("set values or choose a pre-set model then press simulate button");
            compteur = 0;
            win.setTotalProgress(0);
            // there is a waiting loop (for entering new values in the panel)
            while (patient) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Simulation.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
           
            // get values from the panel
            double totalTime = win.getTotalTime();
            int gammaLoop = getNbOfLoop(win.getGammaStart(), win.getGammaEnd(), win.getGammaStep());
            int deltaLoop = getNbOfLoop(win.getDeltaStart(), win.getDeltaEnd(), win.getDeltaStep());
            double gammaStep = win.getGammaStep();
            double deltaStep = win.getDeltaStep();
            // a little tricky condition
            if (gammaStep == 0) {
                gammaStep = 0.1;
            }
            if (deltaStep == 0) {
                deltaStep = 0.1;
            }
            double gamma = win.getGammaStart();
            double delta = win.getDeltaStart();
            double alpha = win.getAlphaStart();
            double beta = win.getBetaStart();
            double gammaEnd = win.getGammaEnd();
            double deltaEnd = win.getDeltaEnd();
            double pulse1Start = win.getPulse1Start();
            double pulse1End = win.getPulse1End();
            double pulse2Start = win.getPulse2Start();
            double pulse2End = win.getPulse2End();
            
            // creation of epilepticTable that will stock results from analyser
            int epilepticTable[][] = new int[gammaLoop][deltaLoop];
           
            // build a new file where stock results
            String root = "Results_" + number + "/";
            File file = new File(root);
            boolean exists = file.exists();
            while(exists){
                number++;
                root = "Results_" + number + "/";
                file = new File(root);
                exists = file.exists();
            }
            new File(root).mkdir();
            
            /* Start simulation loops */
            
            /* //these two following lines doesn't work well (i don't know why! it's an very evil fraction trouble)    
            for (double i = gamma; i <= gammaEnd; i += gammaStep) {   
                for (double j = delta; j <= deltaEnd; j += deltaStep) {
             */
            
            int i=0, j=0;  // temp vars for step values (and used for index of epilepticTable
            double gammaTemp = gamma, deltaTemp = delta;  // temp vars for gamma and delta values
 
            while (i< gammaLoop){
                while(j< deltaLoop){
                    
                    win.setInfoLabel("g:" + gammaTemp + " d:" + deltaTemp + " rk4 resolution...");
                    compteur++;
                    
                    // inits for omega
                    Omega omega = new Omega();
                    Omega.initValues(gammaTemp, deltaTemp, alpha, beta, pulse1Start, pulse1End, pulse2Start, pulse2End);
                    
                    // Some string used by the path
                    String name = createNameOfFile(gammaTemp, deltaTemp, alpha, beta, pulse1Start, pulse1End, pulse2Start, pulse2End, totalTime);
                    String path = name + "/";
                    String nameOfFile = name + ".txt";
                    // Create Path
                    new File(root + path).mkdir();
                    
                    // begin resolution
                    Span timex = new Span(0.0D, totalTime);
                    double[] init = {0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D};
                    Btableau bt = new Btableau("erk4");
                    String s1 = new String(root + path + nameOfFile);
                    String s2 = new String("Stats_Off");
                    int nPoints = (int) (totalTime / step);
                    Erk.erk(omega, timex, init, step, bt, s1, s2, nPoints);
                    
                    // if "export to .png" have been selected
                    if (win.getWriteCheck()) {
                        win.setInfoLabel("g:" + gammaTemp + " d:" + deltaTemp + " plotting ");
                        // pass the name of simulation to the Analyser ( whitch create png from datas )
                        Analyser analyser = new Analyser(root + path, name);
                        epilepticTable[i][j] = analyser.isEpileptic();
                        // and finally delete the very big .txt file
                        File deleteFile = new File(root + path + nameOfFile);
                        deleteFile.delete();
                    }
                    win.setTotalProgress(compteur * 100 / (gammaLoop * deltaLoop));// update progress bar
                
                    j++; deltaTemp = j*deltaStep+ delta;    // up j and deltaTemp
                }// end of j loop
                
                i++; gammaTemp = i*gammaStep +gamma; j=0;   // up i and gammaTemp, reset j
            }// end of i loop
            
            // export the epilepticTable into a xls file
            new XlsExporter(root, epilepticTable, gamma, gammaStep, delta, deltaStep);
            setPatient();
        }
    }

    public static double getStep() {
        return step;
    }

    public static void setPatient() {
        if (patient) {
            patient = false;
        } else {
            patient = true;
        }
    }

    public int getNbOfLoop(double start, double End, double step) {
        int temp = 1;
        if (End != 0) {
            temp = (int) (((1000 * End) - (1000 * start)) / (1000 * step)) + 1;
            //System.out.println("LOOP : " + temp);
            return temp;
        } else {
            //System.out.println("LOOP : " + temp);
            return temp;
        }

    }

    public String createNameOfFile(double gamma,
            double delta,
            double alpha,
            double beta,
            double start,
            double end,
            double startBis,
            double endBis,
            double tTime) {
        NumberFormat nbFormat = NumberFormat.getInstance();
        nbFormat.setMinimumFractionDigits(4); //nb de chiffres apres la virgule

        //donne la chaine representant le double avec 2 chiffres apres la virgules
        if (endBis == 0) {
            return "G" + nbFormat.format(gamma) + "_D" + nbFormat.format(delta) +
                    "_A" + nbFormat.format(alpha) + "_B" + nbFormat.format(beta) +
                    "_Ps" + (int) (start) + "_Pe" + (int) (end) + "_TT" + (int) (tTime);
        } else {
            return "G" + nbFormat.format(gamma) + "_D" + nbFormat.format(delta) +
                    "_A" + nbFormat.format(alpha) + "_B" + nbFormat.format(beta) +
                    "_Ps" + (int) (start) + "_Pe" + (int) (end) + "_P2s" + (int) (startBis) + "_P2e" + (int) (endBis) + "_TT" + (int) (tTime);
        }

    }
}
