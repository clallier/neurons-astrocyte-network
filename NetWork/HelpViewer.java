package NetWork;

import java.awt.BorderLayout;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
 
@SuppressWarnings("serial")
public class HelpViewer extends JFrame 
                            
{
  // Composant Swing permettant de visualiser un document
  private static JEditorPane viewer       = new JEditorPane ();
  
 
  public HelpViewer () 
  {	
        try {
            //this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            //this.setSize(640, 480);
            //this.setVisible(true);
            // Construction de l'Interface Graphique
            JScrollPane scrollPane = new JScrollPane(viewer);
            // Ajout des composants à la fenêtre
            getContentPane().add(scrollPane, BorderLayout.CENTER);

            // Mode non editable
            viewer.setEditable(false);

            URL adrss = new URL("file:///" + System.getProperty("user.dir") + "/Help/Help.html");
            loadPage(adrss.toString());
        } catch (MalformedURLException ex) {
            Logger.getLogger(HelpViewer.class.getName()).log(Level.SEVERE, null, ex);
        }
  }
 
  
        
  public void loadPage (String urlText)
  {
    try 
    {
	
     viewer.setPage (new URL (urlText));
	
    } 
    catch (IOException ex) 
    {
      System.err.println ("Acces impossible a " + urlText);
    }
  }
  
}
