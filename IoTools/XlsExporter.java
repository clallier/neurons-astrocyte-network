/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package IoTools;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

/**
 *
 * @author pipboy
 */
public class XlsExporter {

    /**
     * constructor
     */
    public XlsExporter(String path, int[][] epilepticTable, double gamma, double gammaStep, double delta, double deltaStep) {

        // create output file
        FileOutputStream outFile = null;
        try {

            // create a workbook and a sheet
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet1 = wb.createSheet("Epileptic states");
            
            // create styles and colors
            HSSFCellStyle style0 = wb.createCellStyle();
            style0.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
            style0.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                    
            HSSFCellStyle style1 = wb.createCellStyle();
            style1.setFillForegroundColor(HSSFColor.LIGHT_ORANGE.index);
            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            
            HSSFCellStyle style2 = wb.createCellStyle();
            style2.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
            style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            
            HSSFCellStyle style3 = wb.createCellStyle();
            style3.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
            style3.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            
            
            //column (deltas values)
            for (int i = 0; i < epilepticTable[0].length; i++) {
                HSSFRow rowC = sheet1.createRow((short)(i+1));
                rowC.createCell((short)0).setCellValue(delta+i*deltaStep);
            }
            
            //line (gamma values)
            HSSFRow rowL= sheet1.createRow((short) 0);
            for (int j = 0; j < epilepticTable.length; j++){
                    rowL.createCell((short) (j+1)).setCellValue(gamma+j*gammaStep);
            }
            
            
            // fill row by row (deltas)
            for (int i = 0; i < epilepticTable[0].length; i++) {
              HSSFRow row = sheet1.createRow((short) i + 1);
              
                // fill cells (gammas)
                for (int j = 0; j < epilepticTable.length; j++){
                 
                    // create cell
                    HSSFCell cell = row.createCell((short) (j+1));
                    
                    // set up a color
                    switch(epilepticTable[j][i]){
                        case 1: cell.setCellStyle(style1); break;   // if epileptic use this style 
                        case 2: cell.setCellStyle(style2); break;   // if epileptic but n4 don't spike, use this one
                        case 3: cell.setCellStyle(style3); break;   // if epileptic but n3(/n5?) don't spike, use this one
                        default: cell.setCellStyle(style0); break;  //else (no epileptic pattern) this one
                    }
                    cell.setCellValue(epilepticTable[j][i]);
                }
            }

            // create file, write it, and close it
            outFile = new FileOutputStream(path + "sim_results.xls");
            wb.write(outFile);
            outFile.close();


        // catch io troubles
        } catch (IOException ex) {
            Logger.getLogger(XlsExporter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
