/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package IoTools;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author pipboy2000
 */
public class Analyser {
    private int s1 = 0, s2 = 0, s3 = 0, s4 = 0, s5 = 0, sc = 0;
    /**
     * constructor
     * TODO pass :
     * plotting step
     * 
    // TODO change series names
    // TODO count spikes
    // TODO spikes stats and chart
    // TODO nice fancy CombinedChart
    // TODO del french comments
     */
    public Analyser(String path,String name) {
        try {

            /* init vars */
            String file = path + name +".txt";
            int dimension = 0;   // dimension of solution
            int solLength = 0; // length of solution
            String inputLine;   // String input variable
            double inputDbl;   // double to convert above String to 
            StringTokenizer tokenizer;   // used to tokenize an input line at addedLines time
            String inputStr;   // addedLines token from input line

            /* get solution dimension (num of equation -1) */
            dimension = computeSolDimension(file);
            /*get number of lines */
            solLength = computeSolLength(file);

            /* init series and dataset */
            XYSeries series[] = null;
            XYSeriesCollection dataset[] = null;

            series = new XYSeries[dimension];
            dataset = new XYSeriesCollection[dimension];
            for (int j = 0; j < dimension; j++) {
                series[j] = new XYSeries("Plot" + j);
                dataset[j] = new XYSeriesCollection();
            }
            // change names of main series
            series[0] = new XYSeries("v1");
            series[3] = new XYSeries("v2");
            series[6] = new XYSeries("v3");
            series[9] = new XYSeries("v4");
            series[12] = new XYSeries("c");

            // fill series with file content
            FileReader reader = new FileReader(file);   // initialize readers
            BufferedReader in = new BufferedReader(reader);
            double t = 0.0; // time vector
			int addedLines = 0; // number of datas used for plot

            // spikes test (s*fl1 = flag for spike of neuron '*' superior to 1.0 )
            boolean s1fl1 = false, s2fl1 = false, s3fl1 = false, s4fl1 = false, scfl1 = false;
//            boolean s1fl1 = false, s2fl1 = false, s3fl1 = false, s4fl1 = false, s5fl1 = false, scfl1 = false;

            // takes line by line
            for (int i = 0; i < solLength; i++) {
                inputLine = in.readLine();  // stock addedLines line
                tokenizer = new StringTokenizer(inputLine, " ");    // parse this line by string


                for (int d = 0; d < dimension; d++) {   // for all the dimensions
                    inputStr = tokenizer.nextToken();   // get next token from tokenizer
                    inputDbl = Double.parseDouble(inputStr);   // convert it to double
                    /* count spikes */
                    if (d == 1) {   // for v1
                        if (inputDbl > 1) {
                            s1fl1 = true;
                        }
                        if ((inputDbl <= 1) && (s1fl1)) {
                            s1++;
                            s1fl1 = false;
                        }
                    }
                    if (d == 4) {   // for v2
                        if (inputDbl > 1) {
                            s2fl1 = true;
                        }
                        if ((inputDbl <= 1) && (s2fl1)) {
                            s2++;
                            s2fl1 = false;
                        }
                    }
                    if (d == 7) {   // for v3
                        if (inputDbl > 1) {
                            s3fl1 = true;
                        }
                        if ((inputDbl <= 1) && (s3fl1)) {
                            s3++;
                            s3fl1 = false;
                        }
                    }
                    if (d == 10) {   // for v4
                        if (inputDbl > 1) {
                            s4fl1 = true;
                        }
                        if ((inputDbl <= 1) && (s4fl1)) {
                            s4++;
                            s4fl1 = false;
                        }
                    }
                    if (d == 13) {   // for c
                        if (inputDbl > 1) {
                            scfl1 = true;
                        }
                        if ((inputDbl <= 1) && (scfl1)) {
                            sc++;
                            scfl1 = false;
                        }
                    }

                    /* fill series */
                    if ((i % 20) == 0) {   // speed up analyse : toutes les 50 lignes => TODO 20
                        if (d == 0) {  //dim 0 = t
                            t = inputDbl;
                        } else {
                            // ! series d-1 
                            series[d - 1].add(t, inputDbl); // put inputDbl in the position i of the series
                        }
                    }
                    addedLines++;
                }
            }

            reader.close();   // close reader

            /* fill datasets with series */
            for (int j = 0; j < dimension; j++) {
                dataset[j].addSeries(series[j]);
            }

            JFreeChartBuilder(path,name, "v1", "t", "mV", dataset[0]);
            JFreeChartBuilder(path,name, "v2", "t", "mV", dataset[3]);
            JFreeChartBuilder(path,name, "v3", "t", "mV", dataset[6]);
            JFreeChartBuilder(path,name, "v4", "t", "mV", dataset[9]);
            JFreeChartBuilder(path,name, "c", "t", "mV", dataset[12]);
            /* build a bar chart with spikes values */
            DefaultCategoryDataset spikeDataset = new DefaultCategoryDataset();
            spikeDataset.addValue(s1, "spikes", "neuron 1");
            spikeDataset.addValue(s2, "spikes", "neuron 2");
            spikeDataset.addValue(s3, "spikes", "neuron 3");
            spikeDataset.addValue(s4, "spikes", "neuron 4");

            spikeDataset.addValue(sc, "spikes", "astrocyte");
            JFCBarBuilder(path, name, "Numbers of fires by cells", "Cells", "Spikes", spikeDataset);

            /* and a final combined one */
            XYItemRenderer renderer[] = new XYLineAndShapeRenderer[5];
            for(int j = 0; j<5; j++){
                renderer[j] = new XYLineAndShapeRenderer(true,false);
            }
            
            renderer[0].setSeriesPaint(0, new Color(60,90,255));
            renderer[1].setSeriesPaint(0, new Color(60,130,255));
            renderer[2].setSeriesPaint(0, new Color(60,170,255));
            renderer[3].setSeriesPaint(0, new Color(255,130,60));
            renderer[4].setSeriesPaint(0, Color.DARK_GRAY);
            
            NumberAxis rangeAxis0 = new NumberAxis("n1");
            NumberAxis rangeAxis1 = new NumberAxis("n2");
            NumberAxis rangeAxis2 = new NumberAxis("n3");
            NumberAxis rangeAxis3 = new NumberAxis("n4");
            NumberAxis rangeAxis4 = new NumberAxis("c");
            
            //rangeAxis.setAutoRangeIncludesZero(true);
            XYPlot subplot0 = new XYPlot(dataset[0], null, rangeAxis0, renderer[0]);
            XYPlot subplot1 = new XYPlot(dataset[3], null, rangeAxis1, renderer[1]);
            XYPlot subplot2 = new XYPlot(dataset[6], null, rangeAxis2, renderer[2]);
            XYPlot subplot3 = new XYPlot(dataset[9], null, rangeAxis3, renderer[3]);
            XYPlot subplot4 = new XYPlot(dataset[12], null, rangeAxis4, renderer[4]);

            CombinedDomainXYPlot combiPlot = new CombinedDomainXYPlot(new NumberAxis("time"));
            combiPlot.setGap(5);
            combiPlot.add(subplot0);
            combiPlot.add(subplot1);
            combiPlot.add(subplot2);
            combiPlot.add(subplot3);
            combiPlot.add(subplot4);
            JFreeChart combiChart = new JFreeChart("Summary of neuron behaviouring with "+name, JFreeChart.DEFAULT_TITLE_FONT, combiPlot, true);
            PNGExporter(path, name+"_summary", combiChart);
            
            System.out.println("s1:" + s1 + " s2:" + s2 + " s3:" + s3 + " s4:" + s4 + " s5:" + s5 + " sc:" + sc);
          
            //catch exeptions    
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Analyser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Analyser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public int isEpileptic(){
          //is epileptic? ( an ugly test!)
            if( (s2>(10*s1)) || (s3>(10*s1)) || (s4>(10*s1)) ){ // have to adjust it.
//            if( (s2>(10*s1)) || (s3>(10*s1)) || (s4>(10*s1)) || (s5>(10*s1)) ){ // have to adjust it.
                if(s4<s1){  //if n4 does not pulse anymore
                    return 2;   //
                }else if(s3<s1){  //if n3 (often with n5) does not pulse anymore
                    return 3;   //
                }else{      //and if all pulse profils are in a standard epileptic pattern
                    return 1;
                }
            }else{
                return 0;// else not...
            }
    }

    /**
     * JFreeChartBuilder : build addedLines 2Dplot from addedLines dataset
     * @param title : the title
     * @param xaxis : xaxis label
     * @param yaxis : yaxis label
     * @param dataset : the dataset to plot
     * @return addedLines chart
     */
    private JFreeChart JFreeChartBuilder(String path, String name, String title, String xaxis, String yaxis,
            XYSeriesCollection dataset) throws IOException {
        JFreeChart chart = ChartFactory.createXYLineChart(
                title, //the title 
                xaxis, // x axis label 
                yaxis, // y axis label 
                dataset, // dataset 
                PlotOrientation.VERTICAL,
                true, // legend
                true, // tooltips
                false);             // urls
        // TODO : customisations

        /* finally export it as png */
        PNGExporter(path, name + "_" + title, chart);

        return chart;
    }

    private JFreeChart JFCBarBuilder(String path, String name, String title, String xaxis, String yaxis,
            DefaultCategoryDataset dataset) throws IOException {
        JFreeChart spikesChart = ChartFactory.createBarChart(
                title, // chart title
                xaxis, // domain axis label
                yaxis, // range axis label
                dataset, // data
                PlotOrientation.VERTICAL, // orientation
                true, // include legend
                true, // tooltips?
                false // URLs?
                );
        PNGExporter(path, name + "_spikes", spikesChart);
        return spikesChart;
    }

    /**
     * PNGExporter
     * Permit to export a chart into a png file
     * @param name
     * @param chart
     * @throws java.io.IOException
     */
    private void PNGExporter(String path, String name, JFreeChart chart) throws IOException {
        ChartUtilities.saveChartAsPNG(
                new File(path + name + ".png"), //file TODO voir avec Arnaud
                chart, //the chart
                800, //width 
                600, //height
                null, //chart rendering info null:)
                true,//alpha 
                9);//compression 1-9
    }

    /**
     * computeSolDimension
     * compute the solution dimension : numbers of equations
     * @param String file : url to the file to read
     * @return int : 
     */
    private int computeSolDimension(String file) throws FileNotFoundException, IOException, Exception {
        FileReader r = new FileReader(file);   // initialize reader and
        BufferedReader i0 = new BufferedReader(r);   // buffered reader

        String inputLine = i0.readLine();   // amount of tokens on first line will
        StringTokenizer tokenizer = new StringTokenizer(inputLine, " ");   // be one less than
        int dimension = tokenizer.countTokens() - 1;   // the dimension

        if (dimension < 1) // if dimension is <1, there has to be an error:
        {
            throw new Exception("File has less than one dimension.");   // notify user
        }
        r.close();
        i0.close();
        return dimension;
    }

    /**
     * computeSolLength
     * compute le solution length : numbers of lines in the sol file
     * @param File
     * @return
     */
    private int computeSolLength(String file) throws FileNotFoundException, IOException {
        /* get length of solution file */

        boolean eof = false;   // whether it is end of file or not
        int solLength = 0;   // length of solution array to be calculated

        FileReader r = new FileReader(file);   // initialize reader and
        BufferedReader i1 = new BufferedReader(r);   // buffered reader

        while (!eof) // keep looping until end of file
        {
            if (i1.readLine() != null) // read addedLines line, if it's not null
            {
                solLength++;
            } // (which means not at eof), count it
            else // else it is eof, close the reader and halt the loop
            {
                r.close();
                eof = true;
            }
        }
        return solLength;
    }
}







